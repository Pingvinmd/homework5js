 
// метод об’єкта дає доступ до інформації що в середині об’єкта
//довільний тип 
// коли ми робимо зміну об’єктом у змінну зберігається посилання на ці дані.

  
  
  function createNewUser() {
    let firstName = prompt("Введіть ваше імя");
    let lastName = prompt("Введить вашу фамілію");

    const newUser = Object.create(
        {
            getLogin() {
                return (this.firstName.charAt(0) + this.lastName).toLowerCase();
            },
        },
        {
            firstName: {
                value: firstName,
                writable: false,
                configurable: true,
            },
            lastName: {
                value: lastName,
                writable: false,
                configurable: true,
            },
        }
    );
    return newUser;
}

const user = createNewUser();

console.log(user.getLogin());